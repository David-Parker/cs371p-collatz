// ----------------------------
// projects/collatz/Collatz.c++
// Copyright (C) 2015
// Glenn P. Downing
// ----------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

int cache[1000000] = {};

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    // <your code>
    /* Only integers between 0 and 1000000 */
    assert(i > 0);
    assert(j > 0);
    assert(i < 1000000);
    assert(j < 1000000);

    int max = 1;

    /* Swap i and j so that we can always run linearly through the for loop */
    if(i > j) {
        int temp = i;
        i = j;
        j = temp;
    }

    /* Find the maximum cycle length, but disregard implementation details of cycle_length() */
    for(int k = i; k <= j; k++) {
        int t;
        if(cache[k] > 0) {
            t = cache[k];
        } 
        else {
            t = cycle_length(k);
            cache[k] = t;
        }
        if(t > max)
            max = t;
    }

    assert(max > 0);

    return max;
}

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        /* First check if this n is already within our 1mil elem cache */
        if(n < (int)((sizeof(cache)/sizeof(int)) - 1) && cache[n] > 0) return cache[n] + c - 1;
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);}}
