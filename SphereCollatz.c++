// -------------------------------
// projects/collatz/RunCollatz.c++
// Copyright (C) 2015
// Glenn P. Downing
// -------------------------------

// -------
// defines
// -------

#ifdef ONLINE_JUDGE
    #define NDEBUG
#endif

// --------
// includes
// --------

#include <iostream> // cin, cout, endl, istream, ostream
#include <cassert>  // assert
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

using namespace std;

int cache[1000000] = {};

// -----------
// definitions
// -----------

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param s a string
 * @return a pair of ints, representing the beginning and end of a range, [i, j]
 */
pair<int, int> collatz_read (const string& s);

// ------------
// collatz_eval
// ------------

/**
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @return the max cycle length of the range [i, j]
 */
int collatz_eval (int i, int j);

// ------------
// cycle_length
// ------------

/**
 * @param n the integer of the cylce length to compute.
 * @return the cycle length of n.
 */
int cycle_length (int n);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param w an ostream
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @param v the max cycle length
 */
void collatz_print (ostream& w, int i, int j, int v);

// -------------
// collatz_solve
// -------------

/**
 * @param r an istream
 * @param w an ostream
 */
void collatz_solve (istream& r, ostream& w);

// ----
// main
// ----

int main () {
    collatz_solve(cin, cout);
    return 0;}

pair<int, int> collatz_read (const string& s) {
	istringstream sin(s);
	int i;
	int j;
	sin >> i >> j;
	return make_pair(i, j);}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    // <your code>
    /* Only integers between 0 and 1000000 */
    assert(i > 0);
    assert(j > 0);
    assert(i < 1000000);
    assert(j < 1000000);

    int max = 1;

    /* Swap i and j so that we can always run linearly through the for loop */
    if(i > j) {
        int temp = i;
        i = j;
        j = temp;
    }

    /* Find the maximum cycle length, but disregard implementation details of cycle_length() */
    for(int k = i; k <= j; k++) {
        int t;
        if(cache[k] > 0) {
            t = cache[k];
        } 
        else {
            t = cycle_length(k);
            cache[k] = t;
        }
        if(t > max)
            max = t;
    }

    assert(max > 0);

    return max;
}

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if(n < (int)((sizeof(cache)/sizeof(int)) - 1) && cache[n] > 0) return cache[n] + c - 1;
        if ((n % 2) == 0)
            n = (n / 2);
        else
            n = (3 * n) + 1;
        ++c;}
    assert(c > 0);
    return c;}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);}}

/*
% g++-4.7 -pedantic -std=c++11 -Wall Collatz.c++ RunCollatz.c++ -o RunCollatz



% cat RunCollatz.in
1 10
100 200
201 210
900 1000



% RunCollatz < RunCollatz.in > RunCollatz.out



% cat RunCollatz.out
1 10 1
100 200 1
201 210 1
900 1000 1



% doxygen -g
// That creates the file Doxyfile.
// Make the following edits to Doxyfile.
// EXTRACT_ALL            = YES
// EXTRACT_PRIVATE        = YES
// EXTRACT_STATIC         = YES
// GENERATE_LATEX         = NO



% doxygen Doxyfile
// That creates the directory html/.
*/
